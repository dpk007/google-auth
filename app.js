const express = require('express');
const app = express();
const PORT = 3000;
const passport = require('passport');
const GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
const path = require('path');


app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

// Use the GoogleStrategy within Passport.
//   Strategies in Passport require a `verify` function, which accept
//   credentials (in this case, an accessToken, refreshToken, and Google
//   profile), and invoke a callback with a user object.
passport.use(new GoogleStrategy({
    clientID: 'slkjslk',
    clientSecret: 'sklmksmklamkla',
    callbackURL: "/auth/google/callback"
  },
  function(accessToken, refreshToken, profile, done) {
        console.log(profile);
        //save information in database
      //  User.findOrCreate({ googleId: profile.id }, function (err, user) {
      //    return done(err, user);
      //  });
  }
));

// GET /auth/google
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  The first step in Google authentication will involve
//   redirecting the user to google.com.  After authorization, Google
//   will redirect the user back to this application at /auth/google/callback
app.get('/auth/google',
  passport.authenticate('google', { scope: ['https://www.googleapis.com/auth/plus.login'] }));

// GET /auth/google/callback
//   Use passport.authenticate() as route middleware to authenticate the
//   request.  If authentication fails, the user will be redirected back to the
//   login page.  Otherwise, the primary route function function will be called,
//   which, in this example, will redirect the user to the home page.
app.get('/auth/google/callback', 
  passport.authenticate('google', { failureRedirect: '/login' }),
  function(req, res) {
    res.redirect('/');
  });

  app.get('/', (req, res)=>{
    res.render('index');
  })




app.listen(PORT, () =>{
  console.log(`app is running on port ${PORT}`)
})